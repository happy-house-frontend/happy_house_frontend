$(document).on 'page:change', ->
  $('.selectpicker').selectpicker()

  carousel = (el) ->
    $(el).owlCarousel
      items: 6
      dots: false
      lazyLoad: true
      nav: true
      navRewind: false
      navText: [
        "<i class='fa fa-angle-left'></i>"
        "<i class='fa fa-angle-right'></i>"
      ]
      slideBy: '6'
      margin: 10

  suspendFilter = false

  filter = (el) ->
    return if suspendFilter

    tr = $(el).parents('tr')
    data =
      category: tr.find('select[name=category]').val()
      brand: tr.find('select[name=brand]').val()
      sku: tr.find('input[name=sku]').val()

    nextTr = tr.next('.related-products')
    wrap = nextTr.find 'td'
    
    $.ajax Routes.products_materials_path(),
      method: 'GET'
      data: data

    .done (html) ->
      carousel wrap.html(html).find('.owl-carousel')
      nextTr.show()

  materials = $ '.material-list'

  cascade = (self, url) ->
    self = $ self
    selfName = self.attr 'name'
    targetName = (if selfName is 'category' then 'brand' else 'category')

    tr = self.parents 'tr'
    target = tr.find "select[name=#{targetName}]"

    old = target.val()

    target.prop 'disabled', true
    target.selectpicker 'refresh'

    (data = {})[selfName] = self.val()

    $.ajax url,
      method: 'GET'
      data: data

    .done (json) ->
      exist = false

      options = "<option>请选择</option>"
      for {id, name} in json
        exist = true if old is "#{id}"
        options += "<option value=#{id}>#{name}</option>" 

      target.html(options)
      target.val old if exist

      setTimeout -> 
        target.selectpicker 'refresh'
      , 50

    .fail (xhr) -> alert '数据请求失败，请重试'

    .always -> 
      target.prop 'disabled', false
      target.selectpicker 'refresh'

  materials.on 'change', 'select.material-category', -> 
    cascade @, Routes.brands_materials_path()
    filter @
  .on 'change', 'select.material-brand', ->
    cascade @, Routes.categories_materials_path()
    filter @
  .on 'keyup', 'input.material-sku', ->
    sku = $(@).val()
    filter @ if !sku or sku.length > 4
  .on 'click', '.selected-product', ->
    $(@).parents('tr').next('tr').toggle()
  .on 'change', 'input.selected-image', ->
    self = $ @
    tr = self.parents 'tr'
    current = tr.find "input.selected-image:checked"
    prevTr = tr.prev 'tr' 
    prevTr.find('.selected-product').html """
      #{current.data 'name'} <i class="caret"></i>
    """
    prevTr.find('input.material-sku').val current.data('sku')

    tr.hide()
  .on 'ajax:success', '.remove-material-item', -> $(@).parents('tbody').remove()

  $('.btn-add-material').on 'ajax:success', (e, html) ->
    $('.material-table').append(html).find('.selectpicker').selectpicker()

