module ApplicationHelper
  def active_menu(name)
    controller.menu = name
  end

  def current_menu?(name)
    controller.menu.to_s == name.to_s
  end

  def nav_link(text, url, name:, **options)
    content_tag :li, class:  current_menu?(name) ? 'active' : '' do
      link_to url, options do
        concat text
        yield if block_given?
      end
    end
  end
end
