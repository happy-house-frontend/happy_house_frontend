class ChangeToMany2Many < ActiveRecord::Migration
  def change
    change_table :brands do |t|
      t.remove :category_id
    end

    create_join_table :categories, :brands
  end
end
