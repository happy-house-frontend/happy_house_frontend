# == Schema Information
#
# Table name: categories
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Category < ActiveRecord::Base
  has_and_belongs_to_many :brands

  scope :by_brand, -> id do
    if id.to_i == 0
      all
    else
      joins(:brands).where brands: {id: id}
    end
  end
end
