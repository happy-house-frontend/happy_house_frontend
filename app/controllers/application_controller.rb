class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper :application

  class_attribute :menu, instance_predicate: false

  class << self
    def active_menu(name)
      self.menu = name.to_sym
    end
  end

end
