#= require jquery
#= require jquery_ujs
#= require turbolinks
#= require bootstrap
#= require nprogress
#= require nprogress-turbolinks
#= require owl.carousel
#= require PgwSlider
#= require bootstrap-select
#= require bootstrap-datepicker
#= require js-routes
#= require underscore
#= require react
#= require react_ujs
#= require components
#= require backbone
#= require lewoo
#= require_tree ../templates
#= require_tree ./models
#= require_tree ./collections
#= require_tree ./views
#= require_tree ./routers
#= require_tree .

$.extend $.fn.datepicker.defaults, format: 'dd/mm/yyyy', autoclose: true, language: 'zh-CN'

