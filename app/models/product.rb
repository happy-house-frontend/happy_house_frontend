# == Schema Information
#
# Table name: products
#
#  id         :integer          not null, primary key
#  name       :string
#  brand_id   :integer
#  sku        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Product < ActiveRecord::Base
  belongs_to :brand

  has_many :categories, :through => :brand

  scope :category, -> id do
    id.blank? ? all : joins(:categories).where(categories: {id: id}) 
  end

  scope :brand, -> id { id.blank? ? all : where(brand_id: id) }
  
  scope :sku, -> v { where 'sku like ?', "%#{v}%" }

  def self.filter(hash)
    %i[category brand sku].inject(all) { |result, msg| send msg, hash[msg] }
  end
end
