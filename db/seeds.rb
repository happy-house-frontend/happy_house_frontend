# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Category.destroy_all

Brand.destroy_all

5.times do
  category = Category.create name: Faker::Name.name
  10.times do
    brand = category.brands.create name: Faker::Name.name
    15.times do
      brand.products.create name: Faker::Name.name, sku: Faker::Code.ean
    end
  end
end