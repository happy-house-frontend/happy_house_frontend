$(document).on 'page:change', ->
  $(".one-item-slide").owlCarousel
    items: 1
    loop: true
    autoplay: true
    autoHeight: true
    autoplayHoverPause: true

  $(".recommendation .owl-carousel").owlCarousel
    items: 5
    margin: 10
    responsive:
      0:
        items: 1
      400:
        items: 2
      768:
        items: 3
      970:
        items: 4
      1170:
        items: 5
