# == Schema Information
#
# Table name: brands
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Brand < ActiveRecord::Base
  has_and_belongs_to_many :categories
  has_many :products, :dependent => :destroy

  scope :by_category, -> id do
    if id.to_i == 0
      all
    else
      joins(:categories).where categories: {id: id}
    end
  end
end
