class MaterialsController < ApplicationController
  active_menu :material

  def index
    @categories = Category.all
    @brands = Brand.all
  end

  def new
    @categories = Category.all
    @brands = Brand.all
    render partial: 'material'
  end

  def destroy
    head 200
  end

  def categories
    @categories = Category.by_brand params[:brand]
    render json: @categories
  end

  def brands
    @brands = Brand.by_category params[:category]
    render json: @brands
  end

  def products
    @products = Product.filter params
    render partial: 'related_products', object: @products
  end
end
