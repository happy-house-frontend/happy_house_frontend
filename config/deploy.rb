require 'mina/bundler'
require 'mina/rails'
require 'mina/git'
# require 'mina/rbenv'  # for rbenv support. (http://rbenv.org)
require 'mina/rvm'    # for rvm support. (http://rvm.io)
require 'mina/puma'

# Basic settings:
#   domain       - The hostname to SSH to.
#   deploy_to    - Path to deploy into.
#   repository   - Git repo to clone from. (needed by mina/git)
#   branch       - Branch name to deploy. (needed by mina/git)

set :domain, '123.59.55.165'
set :deploy_to, '/home/ubuntu/www/happy_house_frontend'
set :repository, 'git@bitbucket.org:happy-house-frontend/happy_house_frontend.git'
set :branch, 'master'

# For system-wide RVM install.
# set :rvm_path, '/usr/local/rvm/bin/rvm'

# Manually create these paths in shared/ (eg: shared/config/database.yml) in your server.
# They will be linked in the 'deploy:link_shared_paths' step.
set :shared_paths, ['config/database.yml', 'config/application.yml', 'db/production.sqlite3', 'log', 'tmp/pids', 'tmp/sockets', 'public/system', 'public/static']

# Optional settings:
# 建议使用非root部署：`adduser deploy`, `usermod -a -G sudo deploy`
# 如果需要sudo免密码执行服务器上的命令（比如 `sudo service redis-server restart`），请在先到server上执行 `sudo visudo`，将这行
# '%sudo   ALL=(ALL:ALL) ALL' 修改为 '%sudo   ALL=(ALL:ALL) NOPASSWD: ALL'
set :user, 'ubuntu'    # Username in the server to SSH to.
#   set :port, '30000'     # SSH port number.

# SSH forward_agent. 
# Make sure you’ve run `ssh-keyscan -H bitbucket.org >> ~/.ssh/known_hosts` on server 
# And `ssh-add` locally before running `mina deploy`
set :forward_agent, true

# This task is the environment that is loaded for most commands, such as
# `mina deploy` or `mina rake`.
task :environment do
  # If you're using rbenv, use this to load the rbenv environment.
  # Be sure to commit your .ruby-version or .rbenv-version to your repository.
  # invoke :'rbenv:load'

  # For those using RVM, use this to load an RVM version@gemset.
  invoke :'rvm:use[2.2.2@default]'
end

desc "Create Dir"
task :mkdir, [:path] do |_, args|
  path = args.path or fail '请指定需要创建的文件夹路径：`mina mkdir[SUB_PATH]`'
  queue! %[mkdir -p "#{path}"]
  queue! %[chmod g+rx,u+rwx "#{path}"]
end

# Put any custom mkdir's in here for when `mina setup` is ran.
# For Rails apps, we'll make some of the shared paths that are shared between
# all releases.
task :setup => :environment do

  to :before_hook do
    # Put things to run locally before ssh
    queue %[echo "-----> 将部署服务器（#{domain}）加入信任列表"]
    queue %[ssh-keyscan -H #{domain} >> ~/.ssh/known_hosts]
  end

  invoke "mkdir[#{deploy_to}]", reenable: true

  queue %[echo "-----> 创建并链接共享文件夹"]

  shared_paths.each do |path|
    absolute_path = "#{deploy_to}/#{shared_path}/#{path}"
    path_name = Pathname.new absolute_path

    if path_name.extname.empty?
      invoke "mkdir[#{absolute_path}]", reenable: true 
    else
      folder, file = path_name.split
      invoke "mkdir[#{folder}]", reenable: true 
      queue! %[touch "#{absolute_path}"]
    end
  end

  host = repository[/.+@(.+):/, 1]
  queue %[echo "-----> 将版本控制服务器（#{host}）加入信任列表"]
  queue %[ssh-keyscan -H #{host} >> ~/.ssh/known_hosts]
end

desc "Deploys the current version to the server."
task :deploy => :environment do
  to :before_hook do
    # Put things to run locally before ssh
    queue %[echo "-----> 将本机密钥加入 ssh-agent 的高速缓存" && ssh-add] if forward_agent
  end
  deploy do
    # Put things that will set up an empty directory into a fully set-up
    # instance of your project.
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    invoke :'rails:db_migrate'
    invoke :'rails:assets_precompile'
    invoke :'deploy:cleanup'

    to :launch do
      invoke :'puma:restart'
    end
  end
end

desc "Tail server logs"
task :logs do
  queue "tail -f #{deploy_to}/#{shared_path}/log/production.log"
end

desc "Visit the homepage"
task :open do
  to :before_hook do
    queue %[open "http://#{domain}"]
  end
end
# For help in making your deploy script, see the Mina documentation:
#
#  - http://nadarei.co/mina
#  - http://nadarei.co/mina/tasks
#  - http://nadarei.co/mina/settings
#  - http://nadarei.co/mina/helpers

